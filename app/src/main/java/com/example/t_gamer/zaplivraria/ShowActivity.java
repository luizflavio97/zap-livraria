package com.example.t_gamer.zaplivraria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowActivity extends AppCompatActivity {

    @BindView(R.id.textView_titulo) TextView titulo;
    @BindView(R.id.textView_sinopse) TextView sinopse;
    @BindView(R.id.textView_preco) TextView preco;
    @BindView(R.id.imageView) ImageView imagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        ButterKnife.bind(this);
        sinopse.setMovementMethod(new ScrollingMovementMethod());

        Bundle extras = getIntent().getExtras();

        Double d = extras.getDouble("Preco");
        DecimalFormat df = new DecimalFormat("#.00");

        titulo.setText(extras.getString("Titulo"));
        sinopse.setText(extras.getString("Sinopse"));
        preco.setText("R$ " + String.valueOf(df.format(d)));
        Glide.with(this).load(extras.get("URL")).into(imagem);
    }

    @OnClick(R.id.fab_back)
    public void onClickBack(){
        finish();
    }
}
