package com.example.t_gamer.zaplivraria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements ClickRecyclerView_Interface {

    RecyclerAdapter adapter;
    private ArrayList<Livro> listaLivros = new ArrayList<>();
    private Realm realm;
    @BindView(R.id.recyclerview) RecyclerView recyclerView;

    public static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("ZAP Livraria");
        ButterKnife.bind(this);

        Realm.init(this);
        realm = Realm.getDefaultInstance();
        initRealm();
        addDados();

        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        adapter = new RecyclerAdapter(this, listaLivros,this);
        recyclerView.setAdapter(adapter);

    }



    //Listener do FAB
    //Vai para telar de Adicionar

    @OnClick(R.id.fab)
    public void OnClick() {
        Intent i = new Intent(MainActivity.this, AddActivity.class);
        startActivityForResult(i, REQUEST_CODE);
    }

    //Trata a infomração proveniente da tela de Adiconar.

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        try {
            super.onActivityResult(requestCode, resultCode, intent);

            if (requestCode == REQUEST_CODE  && resultCode  == RESULT_OK) {

                Livro l = new Livro();
                l.setTitulo(intent.getStringExtra("Titulo"));
                l.setSinopse(intent.getStringExtra("Sinopse"));
                l.setPreco(Double.parseDouble(intent.getStringExtra("Preco")));
                l.setUrl_capa(intent.getStringExtra("URL"));

                listaLivros.add(l);
                adapter.notifyDataSetChanged();

                realm = null;
                try {
                    realm = Realm.getDefaultInstance();
                    realm.beginTransaction();
                    l = realm.createObject(Livro.class);
                    l.setTitulo(intent.getStringExtra("Titulo"));
                    l.setSinopse(intent.getStringExtra("Sinopse"));
                    l.setPreco(Double.parseDouble(intent.getStringExtra("Preco")));
                    l.setUrl_capa(intent.getStringExtra("URL"));

                    realm.commitTransaction();

                } finally {
                    if(realm != null){
                        realm.close();
                    }
                }
            }

        } catch (Exception ex) {

        }
    }

    //Listener dos elementos da lista

    @Override
    public void onCustomClick(Object object) {
        Livro livro = (Livro) object;
        String titulo = livro.getTitulo();
        Double preco = livro.getPreco();
        String sinopse = livro.getSinopse();
        String url = livro.getUrl_capa();

        Intent i = new Intent(MainActivity.this, ShowActivity.class);
        i.putExtra("Titulo", titulo);
        i.putExtra("Preco", preco);
        i.putExtra("Sinopse", sinopse);
        i.putExtra("URL", url);

        startActivity(i);
    }

    //Adiciona os dados do WebService e do Realm a lista

    private void addDados(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebService.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebService webService = retrofit.create(WebService.class);
        Call<Catalogo> requestCatalogo = webService.catalogo();

        requestCatalogo.enqueue(new Callback<Catalogo>() {
            @Override
            public void onResponse(Call<Catalogo> call, Response<Catalogo> response) {
                if (response.isSuccessful()){
                    Catalogo catalogo = response.body();

                    for(Livro l : catalogo.catalogo){
                        Livro livro = new Livro();
                        livro.setTitulo(l.getTitulo());
                        livro.setPreco(l.getPreco());
                        livro.setSinopse(l.getSinopse());
                        livro.setUrl_capa(l.getUrl_capa());

                        listaLivros.add(livro);
                        adapter.notifyDataSetChanged();
                    }

                    getRealm();

                } else {

                }
            }

            @Override
            public void onFailure(Call<Catalogo> call, Throwable t) {

            }
        });
    }

    //Configuração do Realm
    private void initRealm(){
        RealmConfiguration configuration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(configuration);
    }

    //Pega os dados do Realm
    private void getRealm(){
        realm.beginTransaction();

        RealmResults<Livro> livros = realm.where(Livro.class).findAll();
        if(!livros.isEmpty()){
            for (int i = livros.size() - 1; i >= 0; i--){
                listaLivros.add(livros.get(i));
                adapter.notifyDataSetChanged();
            }
        }

        realm.commitTransaction();

    }

}
