package com.example.t_gamer.zaplivraria;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebService {

    public static final String url = "http://desafioprodepa2018.getsandbox.com/";

    @GET("catalogo")
    Call<Catalogo> catalogo();
}
