package com.example.t_gamer.zaplivraria;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class Livro extends RealmObject {

    @Required
    private String titulo;

    @Required
    private String url_capa;

    @Required
    private String sinopse;

    private double preco;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl_capa() {
        return url_capa;
    }

    public void setUrl_capa(String url_capa) {
        this.url_capa = url_capa;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
