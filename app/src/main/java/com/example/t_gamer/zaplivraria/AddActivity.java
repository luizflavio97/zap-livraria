package com.example.t_gamer.zaplivraria;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddActivity extends AppCompatActivity {

    @BindView(R.id.editText_titulo) EditText titulo;
    @BindView(R.id.editText_preço) EditText preco;
    @BindView(R.id.editText_sinopse) EditText sinopse;
    @BindView(R.id.editText_url) EditText url;
    String st;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);
    }

    //Listener do Adicionar

    @OnClick(R.id.button_Add)
    public void onClickAdd(){
        Intent intent = getIntent();

        st = String.valueOf(titulo.getText());
        intent.putExtra("Titulo", st);

        st = String.valueOf(sinopse.getText());
        intent.putExtra("Sinopse", st);

        st = String.valueOf(Double.parseDouble(preco.getText().toString()));
        intent.putExtra("Preco", st);

        st = String.valueOf(url.getText());
        intent.putExtra("URL", st);

        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.button_cancel)
    public void onClickBack(){
        setResult(RESULT_CANCELED);
        finish();
    }

}
