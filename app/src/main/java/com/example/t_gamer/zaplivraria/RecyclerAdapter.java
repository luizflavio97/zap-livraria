package com.example.t_gamer.zaplivraria;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.util.ArrayList;



public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    public static ClickRecyclerView_Interface clickRecyclerView_interface;
    Context context;
    private ArrayList<Livro> listaLivros;
    private LayoutInflater mInflater;

    RecyclerAdapter(Context context, ArrayList<Livro> list, ClickRecyclerView_Interface clickRecyclerViewInterface) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.listaLivros = list;
        this.clickRecyclerView_interface = clickRecyclerViewInterface;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    //Liga os dados aos textViews
    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        Livro livro = listaLivros.get(i);

        DecimalFormat df = new DecimalFormat("#.00");

        holder.viewPreco.setText(String.valueOf(df.format(livro.getPreco())) + " R$");
        Glide.with(context).load(listaLivros.get(i).getUrl_capa()).into(holder.imageView);
    }

    //Retorna o tamanho da lista
    @Override
    public int getItemCount() {
        return listaLivros.size();
    }


    //Armazena e utiliza views fora da tela
    public class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView viewPreco;
        protected ImageView imageView;

        public ViewHolder(final View itemView) {
            super(itemView);

            viewPreco = itemView.findViewById(R.id.textView_preco);
            imageView = itemView.findViewById(R.id.capaLivro);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    clickRecyclerView_interface.onCustomClick(listaLivros.get(getLayoutPosition()));
                }
            });
        }
    }
}